-- recreate
DROP TABLE my_stuff;
DROP TABLE requires;
DROP TABLE food;
DROP TABLE recipe;
CREATE TABLE food (
        name varchar,
        unit varchar NOT NULL, 
        PRIMARY KEY (name)
);
CREATE TABLE my_stuff (
        name varchar,
        amount float,
        PRIMARY KEY (name),
        FOREIGN KEY (name) REFERENCES food(name)
);
CREATE TABLE recipe (
        name varchar,
        instructions varchar NOT NULL,
        rating integer NOT NULL,
        PRIMARY KEY (name)
);
CREATE TABLE requires (
        r_name varchar NOT NULL,
        f_name varchar NOT NULL,
        amount float NOT NULL,
        PRIMARY KEY (r_name, f_name),
        FOREIGN KEY (f_name) REFERENCES food(name),
        FOREIGN KEY (r_name) REFERENCES recipe(name)
);
INSERT INTO food (name, unit) 
VALUES                         ('long grain rice', 'g'), 
                                ('tomato', 'st'), 
                                ('onion', 'st'), 
                                ('garlic', 'cloves'),
                                ('olive oil', 'ml'),
                                ('tortillas', 'st'),
                                ('basil', 'g'),
                                ('sesame oil', 'ml'),
                                ('brown sugar', 'ml'),
                                ('red chili pepper', 'peppers'),
                                ('broccoli', 'g'),
                                ('soy sauce', 'ml'),
                                ('champion mushrooms', 'g'),
                                ('taco cheese', 'g'),
                                ('green bell pepper', 'g');
INSERT INTO recipe (name, instructions, rating)
VALUES                                ('Mexican Fried Rice', 'do stuff', 10),
                                        ('Mushroom Quesadillas', 'do other stuff', 9),
                                        ('Broccoli Stir Fry', 'to do or not to do, that is the question', 8);
INSERT INTO my_stuff (name, amount)
VALUES                                ('long grain rice', 400),
                                        ('tomato', 2),
                                        ('onion', 3),
                                        ('garlic', 7),
                                        ('red chili pepper', 3),
                                        ('broccoli', 700),
                                        ('olive oil', 400),
                                        ('basil', 50),
                                        ('sesame oil', NULL),
                                        ('brown sugar', NULL),
                                        ('soy sauce', NULL);
INSERT INTO requires (r_name, f_name, amount)
VALUES                                ('Mexican Fried Rice', 'long grain rice', 300),
                                        ('Mexican Fried Rice', 'olive oil', 30),
                                        ('Mexican Fried Rice', 'red chili pepper', 1),
                                        ('Mexican Fried Rice', 'tomato', 2),
                                        ('Mexican Fried Rice', 'onion', 1),
                                        ('Mexican Fried Rice', 'garlic', 2),
                                        ('Mushroom Quesadillas', 'olive oil', 20),
                                        ('Mushroom Quesadillas', 'champion mushrooms', 20),
                                        ('Mushroom Quesadillas', 'tomato', 1),
                                        ('Mushroom Quesadillas', 'tortillas', 20),
                                        ('Mushroom Quesadillas', 'basil', 20),
                                        ('Mushroom Quesadillas', 'green bell pepper', 1),
                                        ('Mushroom Quesadillas', 'taco cheese', 100),
                                        ('Broccoli Stir Fry', 'onion', 1),
                                        ('Broccoli Stir Fry', 'broccoli', 300),
                                        ('Broccoli Stir Fry', 'olive oil', 40),
                                        ('Broccoli Stir Fry', 'sesame oil', 30),
                                        ('Broccoli Stir Fry', 'soy sauce', 20),
                                        ('Broccoli Stir Fry', 'brown sugar', 50),
                                        ('Broccoli Stir Fry', 'long grain rice', 200);


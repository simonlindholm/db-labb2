import System.IO
import Data.List
import Control.Monad
import Database.HDBC
import Database.HDBC.Sqlite3

wordsWhen :: (Char -> Bool) -> String -> [String]
wordsWhen p s =  case dropWhile p s of
                      "" -> []
                      s' -> w : wordsWhen p s''
                            where (w, s'') = break p s'

formatAmount :: Double -> String -> String
formatAmount amount unit =
    (if amount == fromIntegral (round amount)
       then show $ round amount
       else show amount)
    ++ unit

fetchUnit :: Connection -> String -> IO (Maybe String)
fetchUnit conn food = do
    foodRows <- quickQuery' conn "SELECT unit FROM food WHERE name = ?" [toSql food]
    return $ if null foodRows
       then Nothing
       else Just $ fromSql $ head $ head foodRows

listKitchen :: Connection -> IO ()
listKitchen conn = do
    -- It would be nicer to fetch units separately, but hey, this way it's all in a single query.
    rows <- quickQuery' conn "SELECT my_stuff.name, my_stuff.amount, food.unit \
        \ FROM my_stuff \
        \ INNER JOIN food \
        \ ON food.name = my_stuff.name" []
    putStrLn ""
    mapM_ print rows
    where
        print [sName, sAmount, sUnit] =
            putStrLn $
                fromSql sName ++
                maybe "" displayAmount (fromSql sAmount :: Maybe Double)
                where displayAmount amount =
                          ": " ++ formatAmount amount (fromSql sUnit)

askForFoodUnit :: Connection -> (String -> String -> IO ()) -> IO ()
askForFoodUnit conn continuation = do
    putStr "Which food? "
    hFlush stdout
    food <- getLine
    maybeUnit <- fetchUnit conn food
    case maybeUnit of
         Nothing -> putStrLn "Invalid food name."
         Just unit -> continuation food unit

deleteFromKitchen :: Connection -> IO ()
deleteFromKitchen conn =
    askForFoodUnit conn (\food _ -> do
        changedRows <- run conn "DELETE FROM my_stuff WHERE name = ?" [toSql food]
        commit conn
        unless (changedRows == 1) $ putStrLn "You don't have that food")

addToKitchen :: Connection -> IO ()
addToKitchen conn =
    askForFoodUnit conn (\food unit -> do
        let unitMsg = if null unit
                        then ""
                        else " (in " ++ unit ++ ")"
        rows <- quickQuery' conn "SELECT 1 FROM my_stuff WHERE name = ?" [toSql food]
        if not $ null rows
            then putStrLn "Already have that food"
            else do
                putStr ("Amount" ++ unitMsg ++ "? ")
                hFlush stdout
                amountStr <- getLine
                let amount = read amountStr :: Int
                run conn "INSERT INTO my_stuff (name, amount) VALUES (?, ?)" [toSql food, toSql amount]
                commit conn)

canDefinitelyMake :: Connection -> IO [String]
canDefinitelyMake conn = do
    li <- quickQuery' conn " \
    \ SELECT name FROM recipe \
    \ EXCEPT \
    \ SELECT r_name \
    \ FROM \
    \     ( \
    \         SELECT r_name, f_name FROM requires \
    \         EXCEPT \
    \         SELECT requires.r_name, requires.f_name \
    \         FROM my_stuff, requires \
    \         WHERE \
    \             my_stuff.name = requires.f_name AND \
    \             my_stuff.amount IS NOT NULL AND \
    \             requires.amount <= my_stuff.amount \
    \     ) AS missing \
    \ " []
    return $ map (fromSql . head) li

canPerhapsOrDefinitelyMake :: Connection -> IO [String]
canPerhapsOrDefinitelyMake conn = do
    li <- quickQuery' conn " \
    \ SELECT name FROM recipe \
    \ EXCEPT \
    \ SELECT r_name \
    \ FROM \
    \ ( \
    \     SELECT r_name, f_name FROM requires \
    \     EXCEPT \
    \     SELECT requires.r_name, requires.f_name \
    \     FROM my_stuff, requires \
    \     WHERE \
    \         my_stuff.name = requires.f_name AND \
    \         (my_stuff.amount IS NULL OR \
    \         requires.amount <= my_stuff.amount) \
    \ ) AS missing; \
    \ " []
    return $ map (fromSql . head) li

canMake :: Connection -> IO ()
canMake conn = do
    definitely <- canDefinitelyMake conn
    putStrLn ""
    putStrLn "You can definitely make:"
    if null definitely
       then putStrLn "- nothing :("
       else mapM_ (putStrLn . ("- " ++)) definitely

    perhapsOrDefinitely <- canPerhapsOrDefinitelyMake conn
    let canPerhapsMake = perhapsOrDefinitely \\ definitely

    let printPerhapsDoable recipe = do
        toCheck <- quickQuery' conn " \
        \ SELECT missing.f_name, requires.amount, food.unit \
        \ FROM \
        \ ( \
        \     SELECT r_name, f_name FROM requires \
        \     EXCEPT \
        \     SELECT requires.r_name, requires.f_name \
        \     FROM my_stuff, requires \
        \     WHERE \
        \         my_stuff.name = requires.f_name AND \
        \         my_stuff.amount IS NOT NULL \
        \ ) AS missing, requires \
        \ INNER JOIN food \
        \ ON food.name = missing.f_name \
        \ WHERE \
        \     requires.r_name = ? AND \
        \     requires.r_name = missing.r_name AND \
        \     requires.f_name = missing.f_name \
        \ " [toSql recipe]
        let toCheckStrs = map format toCheck
        putStrLn $ "- " ++ recipe ++ " (check: " ++ (intercalate ", " toCheckStrs) ++ ")"
        where
          format [sName, sAmount, sUnit] =
                formatAmount (fromSql sAmount) (fromSql sUnit) ++ " " ++ fromSql sName

    putStrLn ""
    putStrLn "You can possibly make:"
    if null canPerhapsMake
       then putStrLn "- nothing :("
       else mapM_ printPerhapsDoable canPerhapsMake

getShoppingList :: Connection -> [String] -> IO [(String, Double, String)]
getShoppingList conn parts = do
    let recipeSql = intercalate " OR " $ map makeSql parts -- sqlite sadness :(

    list <- quickQuery' conn (" \
    \ SELECT \
    \     requires.f_name AS name, \
    \     (CASE WHEN my_stuff.amount IS NULL THEN requires.amount ELSE requires.amount - my_stuff.amount END) \
    \         AS amount_to_buy, \
    \     food.unit \
    \ FROM requires \
    \ JOIN food \
    \ ON food.name = requires.f_name \
    \ LEFT JOIN my_stuff \
    \ ON requires.f_name = my_stuff.name \
    \ WHERE \
    \     requires.f_name = my_stuff.name AND \
    \     (" ++ recipeSql ++ ") AND \
    \     (requires.amount > my_stuff.amount OR my_stuff.amount IS NULL) \
    \ ") []
    return $ map toTuple list
  where
    makeSql name = "requires.r_name = '" ++ escapeSql name ++ "'"
    escapeSql = concatMap (\x -> if x == '\'' then "''" else [x])
    toTuple [sName, sAmount, sUnit] = (fromSql sName, fromSql sAmount, fromSql sUnit)

makeShoppingList :: Connection -> IO ()
makeShoppingList conn = do
    putStr "Which recipes (comma-separated line, please)? "
    hFlush stdout
    line <- getLine
    let parts = map trim $ splitAtComma line
    list <- getShoppingList conn parts
    putStrLn "You will need to buy:"
    if null list
       then putStrLn "- nothing :)"
       else mapM_ (putStrLn . format) list
  where
    trim = reverse . dropWhile (== ' ') . reverse . dropWhile (== ' ')
    splitAtComma = wordsWhen (== ',')
    format (name, amount, unit) = "- " ++ formatAmount amount unit ++ " " ++ name

updateAfterMaking :: Connection -> IO ()
updateAfterMaking conn = do
    putStr "Recipe name? "
    hFlush stdout
    recipe <- getLine
    -- SQLite is terrible for this, because it doesn't support joins in UPDATE.
    -- We'd really like to use a loop here (in Haskell), but it feels like cheating.
    run conn "\
        \UPDATE my_stuff \
        \SET amount = amount - \
        \    (SELECT requires.amount FROM requires WHERE requires.f_name = my_stuff.name AND requires.r_name = ?) \
        \WHERE EXISTS(SELECT *       FROM requires WHERE requires.f_name = my_stuff.name AND requires.r_name = ?) \
        \ " [toSql recipe, toSql recipe]
    commit conn


loop :: Connection -> IO ()
loop conn = do
    putStrLn "\nWhat to do?"
    let dummy = const (fail "")
    let options = [ ("List foodstuffs in kitchen", listKitchen)
                  , ("Delete from kitchen", deleteFromKitchen)
                  , ("Add to kitchen", addToKitchen)
                  , ("List things you can make", canMake)
                  , ("Create a shopping list", makeShoppingList)
                  , ("Update kitchen after having made a recipe", updateAfterMaking)
                  , ("Quit", dummy) ]
    putStr $ unlines $ zipWith (\num opt -> show num ++ ") " ++ fst opt) [1..] options
    input <- getLine
    let chosen = (read input :: Int) - 1
    if chosen < 0 || chosen >= length options
       then do
           putStrLn "?"
           loop conn
       else
           unless (chosen == length options - 1) $ do
              let handler = snd (options !! chosen)
              handler conn
              loop conn

main:: IO ()
main = handleSqlError $ do
    conn <- connectSqlite3 "lab2.db"
    loop conn
    disconnect conn
